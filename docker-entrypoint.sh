#!/bin/bash
set -e

if [ "$1" = 'couchdb' ]; then
    : ${COUCH_USER:=couchdb}
    : ${COUCH_PASS:=$COUCH_USER}
    echo "Creating user: ${COUCH_USER} with password: ${COUCH_PASS}"
    sed -i "s/user = qwerty/$COUCH_USER = $COUCH_PASS/" /etc/couchdb/local.ini
    chown couchdb:couchdb /db/couchdb -R
    chown couchdb:couchdb /var/log/couchdb -R
    exec su - couchdb -c '/usr/bin/couchdb -a //etc/couchdb/default.ini -a //etc/couchdb/local.ini -b -r 5 -p //var/run/couchdb/couchdb.pid -o /dev/null -e /dev/null -R'
fi
exec "$@"

