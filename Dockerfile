FROM centos:6

MAINTAINER scukonick@gmail.com

RUN yum -y install epel-release && \
    echo -e "[cloudant]\nname=Cloudant Repo\nbaseurl=http://packages.cloudant.com/rpm/\$releasever/\$basearch\nenabled=1\ngpgcheck=0" > /etc/yum.repos.d/cloudant.repo && \
    yum -y install wget js js-devel autoconf automake libtool curl-devel erlang-erts erlang-etap erlang-ibrowse erlang-mochiweb erlang-oauth erlang-os_mon help2man libicu-devel perl-Test-Harness erlang-crypto erlang-erts erlang-inets erlang-kernel erlang-sasl erlang-stdlib erlang-tools gcc-c++ taglib libicu-devel make && \
    yum clean all

RUN yum -y install tar 
RUN wget https://archive.apache.org/dist/couchdb/1.2.1/apache-couchdb-1.2.1.tar.gz -O /tmp/couchdb.tar.gz && \
    cd /tmp && tar xzf /tmp/couchdb.tar.gz && \
    cd apache-couchdb-1.2.1 && \
    ./configure --exec-prefix=/usr --prefix=/ --with-erlang=/usr/lib64/erlang/usr/include --datarootdir=/usr && \
    make && make install 

RUN mv /etc/rc.d/couchdb /etc/init.d/couchdb ; \
    useradd -d /var/lib/couchdb/ -u 600 couchdb ; \
    chown -R couchdb:couchdb /var/lib/couchdb/ ; \
    chown -R couchdb:couchdb /var//run/couchdb/ ; \ 
    chown -R couchdb:couchdb /var/log/couchdb/ ; \
    chown -R couchdb:couchdb /etc/couchdb/ ; \
    chkconfig --level 345 couchdb on   ; 

COPY local.ini /etc/couchdb/local.ini
COPY default.ini /etc/couchdb/default.ini
COPY docker-entrypoint.sh /

RUN mkdir -p /db/couchdb
#RUN mkdir -p /db/couchdb/data /db/couchdb/view_index && chown couchdb /db /etc/couchdb -R
VOLUME /db/couchdb
VOLUME /var/log/couchdb
WORKDIR /db/couchdb
ENV COUCH_USER couchdbx
ENV COUCH_PASS couchdbx
ENTRYPOINT ["/docker-entrypoint.sh"]
EXPOSE 6341
CMD ["couchdb"]
